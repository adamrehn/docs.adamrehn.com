---
title: ue4 conan generate
blurb: Generates UE4 Conan profiles and associated wrapper packages.
layout: command
pagenum: 5
---

{% highlight bash %}
ue4 conan generate [--profile-only] [--remove-only]
{% endhighlight %}

This command generates [UE4 Conan profiles](../read-these-first/concepts#ue4-conan-profiles) and accompanying [wrapper packages](../read-these-first/concepts#wrapper-packages) for the Unreal Engine installation that [ue4cli](../../ue4cli) is currently configured to act as an interface for. Note that you will need to use a source build of the Unreal Engine to generate wrapper packages rather than an [Installed Build](https://docs.unrealengine.com/en-US/Programming/Deployment/UsinganInstalledBuild/index.html) (such as those obtained via the Epic Games Launcher), since Installed Builds do not contain all of the files necessary for wrapper generation. Fortunately, you don't actually need to have built UE4 from source for this to work, you can simply clone the source code and run `Setup.bat`/`Setup.sh` and `GenerateProjectFiles.bat`/`GenerateProjectFiles.sh` to download the required third-party dependencies and conan-ue4cli can generate the wrappers from the source tree. **See the [Installation and setup](../workflow/installation) page for more details on generating profiles and wrapper packages.**

At the present time Conan profiles are only generated for the host system platform, but future versions of conan-ue4cli will support generating profiles for performing cross-compilation to other platforms.

By default, any existing profiles and wrapper packages will be removed and new ones will be generated. You can specify the `--remove-only` flag to perform removal only and skip the generation step, or the `--profile-only` flag to generate the UE4 Conan profiles without generating the accompanying wrapper packages.
