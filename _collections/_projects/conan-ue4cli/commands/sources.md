---
title: ue4 conan sources
blurb: Retrieves the source code of the dependencies for one or more conanfiles.
layout: command
pagenum: 7
---

{% highlight bash %}
ue4 conan sources [-d DIR] PROFILE CONANFILE1 [CONANFILE2 ...]
{% endhighlight %}

This command retrieves the source code for all dependency packages of the specified conanfile(s). Source code is compressed into ZIP archives and placed in the output directory, which defaults to the current working directory if not otherwise specified via the `-d` flag. This is primarily useful for satisfying software license terms which require you to provide the source code of your third-party dependencies when you distribute them in binary form. [Wrapper packages](../read-these-first/concepts#wrapper-packages) will be automatically excluded from the list of dependencies.

The `PROFILE` argument specifies the UE4 Conan profile that should be used to generate the dependency graphs for each of the specified conanfiles. You can specify the value `host` to use the default profile for the host platform and the Unreal Engine version reported by [ue4cli](../../ue4cli).

You can specify one or more paths to conanfiles, and each path may also include the wildcards `*` and `**`, which will be expanded using the Python [glob()](https://docs.python.org/3/library/glob.html) function with recursive matching enabled.
