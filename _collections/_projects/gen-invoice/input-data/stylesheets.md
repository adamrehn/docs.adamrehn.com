---
title:  Stylesheets
pagenum: 4
---

## Description

Stylesheets provide a way of customising the appearance of invoices or quotes when they are displayed in a web browser, or rendered to PDF. Because [templates](./templates) can specify any arbitrary HTML structure that the user desires, stylesheets will typically be designed for compatibility with a specific template or a set of conventions that one or more templates adhere to.

It is worth noting that templates may opt to ignore stylesheets altogether by not including the {% raw %}`{{ css }}`{% endraw %} template tag. If you are encountering issues whereby the CSS from a stylesheet is not being honoured, be sure to check that the template you are using actually includes the stylesheet CSS in its output.


## Data format

{% include
	gen-invoice/formats-table.html
	cli="CSS file"
	api="[str](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str) (string containing CSS code)"
	rpc="*(coming soon)*"
%}


## Examples

You can find the default stylesheet that ships with the command-line interface for gen-invoice here: <https://github.com/adamrehn/gen-invoice/blob/master/gen_invoice/defaults/default.css>

The default stylesheet is designed to be compatible with the default template that ships with the command-line interface for gen-invoice.
