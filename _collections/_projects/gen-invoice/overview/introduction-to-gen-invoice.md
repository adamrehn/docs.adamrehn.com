---
title:  Introduction to gen-invoice
pagenum: 1
---

## Overview

The gen-invoice Python package provides a flexible, template-driven system for generating invoices and quotes in both HTML and PDF format. Great care has been taken to ensure users have complete control over the generated output, by delegating all formatting decisions to templates and minimising the use of predetermined information schemas to the greatest extent feasible.

Invoice generation can be invoked either directly by end users or programmatically by other software components as part of a larger system. The package is designed to cater primarily to individuals and small businesses in the technology industry who would like to automate invoice generation independently of other financial management software or systems.

Template functionality is provided by the [Jinja template engine](http://jinja.pocoo.org/) and PDF generation functionality is provided by [electron-pdf](https://www.npmjs.com/package/electron-pdf) {{ site.data.gen-invoice.dependencies.electron-pdf }} or newer when available.


## Writing templates

Irrespective of the interface that you will use to generate invoices and quotes, you should first familiarise yourself with [how to write invoice templates](./writing-templates).


## Available interfaces

There are a number of different interfaces available for gen-invoice that cater to various usage workflows:

{::nomarkdown}
{% assign interfaces = site.documents | where: "project", page.project | where: "category", "Interfaces" | where_exp: "page", "page.order != null" | sort: "order" %}
<ul class="interfaces">
{% for interface in interfaces %}
	<li><a href="{{ interface.url | relative_url | uri_escape }}"><strong>{{ interface.title | escape }}</strong></a><br>{{ interface.blurb | escape }}</li>
{% endfor %}
</ul>
{:/}


## Links

- [gen-invoice GitHub repository]({{ site.data.common.projects.gen-invoice.repo }})
- [gen-invoice package on PyPI](https://pypi.org/project/gen-invoice/)
