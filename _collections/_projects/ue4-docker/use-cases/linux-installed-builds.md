---
title:  Linux Installed Builds
blurb: >
  Under Windows and macOS, Engine licensees can easily download and manage
  Installed Builds of UE4 through the Epic Games Launcher. Since version 4.21.0
  of the Engine, ue4-docker provides an alternative source of Installed Builds
  under Linux in lieu of a native version of the launcher.
pagenum: 4
---

{% include alerts/info.html content="This page has migrated to the [Unreal Containers community hub](https://unrealcontainers.com/). You can find the new version here: [Use Cases: Linux Installed Builds](https://unrealcontainers.com/docs/use-cases/linux-installed-builds)." %}
