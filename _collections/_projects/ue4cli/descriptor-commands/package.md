---
title:  ue4 package
blurb: Packages a build of the Unreal project or plugin in the current directory.
layout: command
usage_override: "Usage syntax for projects:"
pagenum: 4
---

{% highlight bash %}
{{ page.title | escape }} [CONFIGURATION] [EXTRA UAT ARGS]
{% endhighlight %}

**Usage syntax for plugins:**

{% highlight bash %}
{{ page.title | escape }} [EXTRA UAT ARGS]
{% endhighlight %}

**Description:**

This command packages a build of the Unreal project or plugin located in the current working directory, using common packaging options that should be suitable for most scenarios. (If no `.uproject` or `.uplugin` file can be found in the current directory then ue4cli will emit an error and halt execution.) The packaged build will be placed in a subdirectory called **"dist"** inside the current working directory (short for "distribution".)

When packaging a project, the first optional parameter is used to specify the build configuration to build and package, which can be any of the valid [Unreal Engine build configurations](https://docs.unrealengine.com/en-us/Programming/Development/BuildConfigurations):

- **Debug**
- **DebugGame**
- **Development**
- **Shipping**
- **Test**

If no build configuration is explicitly specified then the **Shipping** configuration will be built and packaged by default.

Note that plugins are always packaged in both **Development** and **Shipping** configurations. **Do not specify a build configuration parameter when packaging a plugin, as this will confuse UAT!**

Any additional arguments that are specified will be passed directly to RunUAT, along with the appropriate default arguments for the type of descriptor (project or plugin) that is being packaged. **Note that a build configuration must be specified when packaging a project and passing additional arguments to RunUAT.**

The default arguments for packaging projects are:

{% highlight bash %}
BuildCookRun
  -platform=<PLATFORM>
  -project=<PROJECT_FILE>
  -clientconfig=<BUILD_CONFIGURATION>
  -serverconfig=<BUILD_CONFIGURATION>
  -noP4
  -cook
  -allmaps
  -build
  -stage
  -prereqs
  -pak
  -archive
  -archivedirectory=<PROJECT_DIR>/dist
{% endhighlight %}

These defaults will automatically adapt to any user-specified arguments:

- The `-platform=<PLATFORM>` argument can be overridden with a user-specified value.
- If the `-server` flag is specified then the platform value will also be used to generate an appropriate `-serverplatform=<PLATFORM>` argument.
- The `-allmaps` flag will only be included if the `-noclient` flag is not present.

The default arguments for packaging plugins are:

{% highlight bash %}
BuildPlugin
  -Plugin=<PLUGIN_FILE>
  -Package=<PROJECT_DIR>/dist
{% endhighlight %}
