---
title:  ue4 run
blurb: Runs the Editor for the Unreal project.
layout: command
pagenum: 5
---

{% highlight bash %}
{{ page.title | escape }} [--debug] [EXTRA ARGS]
{% endhighlight %}

**Description:**

This command runs the Editor for the Unreal project located in the current working directory. (If no `.uproject` file can be found in the current directory then ue4cli will emit an error and halt execution.) Note that the [ue4 build](./build) command must be used to build the Editor for the project before it can be run if the project contains any C++ code. Blueprint-only projects do not have this requirement.

By default the Editor will be invoked with the path to the `.uproject` file for the Unreal project in the current directory, as well as the `-stdout` and `-FullStdOutLogOutput` flags which ensure log output is printed under Windows. Any additional arguments that are specified will be appended to these flags.

The `--debug` flag is an alias for the `-debug` parameter for the Editor, and is provided purely as a convenience.