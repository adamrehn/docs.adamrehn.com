---
title:  ue4 editor
blurb: Runs the editor without an Unreal project (useful for creating new projects).
layout: command
pagenum: 2
---

{% highlight bash %}
{{ page.title | escape }} [EXTRA ARGS]
{% endhighlight %}

**Description:**

This command runs the Editor for the Unreal Engine installation that ue4cli is currently acting as an interface to. This is mostly just useful for using the Editor to create new projects, since the [ue4 run](../project-commands/run) command will run the Editor for any project that already exists. Any additional arguments that are specified will be passed directly to the Editor.
