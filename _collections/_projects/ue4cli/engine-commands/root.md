---
title:  ue4 root
blurb: Prints the path to the root directory of the Unreal Engine installation.
layout: command
pagenum: 3
---

{% highlight bash %}
{{ page.title | escape }}
{% endhighlight %}

**Description:**

This command prints the path to the Unreal Engine installation that ue4cli is currently acting as an interface to. If a path override has been specified via the [ue4 setroot](../configuration-commands/setroot) command then this value will be used, otherwise [auto-detection](../overview/introduction-to-ue4cli#unreal-engine-location-auto-detection) will be performed.
