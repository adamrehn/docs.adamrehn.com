---
title:  ue4 version
blurb: Prints the version string of the Unreal Engine installation.
layout: command
pagenum: 4
---

{% highlight bash %}
{{ page.title | escape }} [major|minor|patch|full|short]
{% endhighlight %}

**Description:**

This command prints the version details for the Unreal Engine installation that ue4cli is currently acting as an interface to. The following output formats are supported:

- **major**: prints just the major version number (i.e. "4")
- **minor**: prints just the minor version number (e.g. "21")
- **patch**: prints just the patch version number (e.g. "0")
- **full**: prints the full version number (e.g. "4.21.0")
- **short**: prints the major and minor version number without the patch number (e.g. "4.21")

If no format name is explicitly specified then the output will default to the **full** version number.
