---
title:  ue4 ldflags
blurb: Prints the linker flags for building against the specified libraries.
layout: command
pagenum: 5
---

{% highlight bash %}
{{ page.title | escape }} [--multiline] [--flagsonly] [--nodefaults] [LIBS]
{% endhighlight %}

**Description:**

This command prints the linker flags required to link against the specified list of libraries. (To determine the available library names, run the [ue4 libs](./libs) command.)

If the `--multiline` flag is specified then each flag will be printed on a separate line. The default is to print all of the flags on a single line, delimited by spaces.

If the `--flagsonly` flag is specified then only the linker flags themselves (e.g. specifying library directories) will be printed without the actual library names at the end. The default is to print the library names too, since these are part of the actual linker command that you would run when linking against the libraries.

If the `--nodefaults` flag is specified when running under Linux then the details for linking against libc++ will not be included in the output. This flag does nothing under macOS and Windows.
