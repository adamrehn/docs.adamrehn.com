---
title:  Creating and using ue4cli plugins
pagenum: 2
---

## Using ue4cli plugins

Plugins for ue4cli take the form of separate Python packages that sit alongside the ue4cli package. When the `ue4` command is run, any installed plugins will be detected automatically, and the plugin-contributed commands will be added to the list of built-in ue4cli commands.

If you were to run the command `ue4 --help` on a system with no ue4cli plugins installed, the last section of the output would look like so:

{% highlight plaintext %}
Commands defined by plugins
---------------------------

These commands are defined by currently installed ue4cli plugins:

	(No commands in this group)

{% endhighlight %}

If you were to then install the [conan-ue4cli]({{ site.data.common.projects.conan-ue4cli.repo }}) plugin by running the command `pip3 install conan-ue4cli`, the last section of the `ue4 --help` output would instead look something like this:

{% highlight plaintext %}
Commands defined by plugins
---------------------------

These commands are defined by currently installed ue4cli plugins:

	conan [SUBCOMMAND] [ARGS] - Invokes conan-ue4cli functionality. Run `ue4 conan` to see the list of supported subcommands.

{% endhighlight %}

This makes it easy to see what commands are provided by any currently installed plugins, since each plugin can define multiple commands. These commands can then be run in exactly the same manner as the built-in ue4cli commands.


## Available ue4cli plugins

**Official plugins:**

- [conan-ue4cli]({{ site.data.common.projects.conan-ue4cli.repo }}): provides functionality for generating and using [Conan](https://conan.io/) packages that wrap the third-party libraries bundled in the `Engine/Source/ThirdParty` subdirectory of the Unreal Engine 4 source tree.

**Community-maintained plugins**:

- [ctags-ue4cli](https://github.com/drichardson/ctags-ue4cli/): provides functionality for building ctags for Unreal Engine projects and plugins using [Universal Ctags](https://github.com/universal-ctags/ctags).


## Creating ue4cli plugins

{% capture _alert_content %}
The example code snippets provided in this section are adapted from the source code of the [conan-ue4cli]({{ site.data.common.projects.conan-ue4cli.repo }}) plugin, which was the first official plugin developed for ue4cli.
{% endcapture %}
{% include alerts/info.html content=_alert_content %}

The ue4cli package utilises the [plugin entrypoint system from setuptools](https://setuptools.readthedocs.io/en/latest/setuptools.html#dynamic-discovery-of-services-and-plugins) for the registration and discovery of plugins. To register your package as a ue4cli plugin, declare an entrypoint named `ue4cli.plugins` in your package's `setup.py` file containing the list of commands provided by your plugin, like so:

{% highlight python %}
entry_points = {
	'ue4cli.plugins': ['conan=conan_ue4cli:__PLUGIN_DESCRIPTOR__']
}
{% endhighlight %}

Each entry in the list is a key/value pair specifying the command name and the descriptor object for that command. A descriptor object is simply a dictionary specifying the command description and argument list for the help output, along with the function that will be called when the command is invoked:

{% highlight python %}
# The function can be called whatever you like, and can even be a lambda, so long as it matches this exact signature
def main(manager, args):
	# Command implementation goes here

# The descriptor object can be called whatever you like, since you specify the name in the entrypoint information in setup.py
__PLUGIN_DESCRIPTOR__ = {
	'action': main,
	'description': 'Invokes conan-ue4cli functionality. Run `ue4 conan` to see the list of supported subcommands.',
	'args': '[SUBCOMMAND] [ARGS]'
}
{% endhighlight %}

The function will be passed two parameters:

- `manager`: an instance of the [UnrealManagerBase](https://github.com/adamrehn/ue4cli/blob/master/ue4cli/UnrealManagerBase.py) class that provides access to the currently detected or configured installation of the Unreal Engine
- `args`: the list of command line arguments with the command itself removed (i.e. `sys.argv[2:]`)

Descriptor objects that do not contain the required dictionary keys or that contain a function with the wrong signature will be ignored by ue4cli during the plugin detection phase, so it is important to match these specifications exactly.
