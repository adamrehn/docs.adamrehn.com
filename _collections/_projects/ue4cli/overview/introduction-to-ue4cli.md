---
title:  Introduction to ue4cli
pagenum: 1
---

## Overview

The ue4cli Python package implements a command-line tool called `ue4` that provides a simplified interface to various functionality of the build system for Epic Games' [Unreal Engine 4](https://www.unrealengine.com/). The primary goals of this tool are as follows:

- Abstract away the platform-specific details of the various batch files and shell scripts scattered throughout the Engine's source tree.
- Provide the ability to easily generate IDE project files under Linux, where no shell integration currently exists to perform this task from outside the Editor.
- Determine the compiler flags required to build third-party libraries for use within Engine modules. This is particularly important under macOS and Linux where [symbol interposition](https://developer.apple.com/library/content/documentation/DeveloperTools/Conceptual/DynamicLibraries/100-Articles/DynamicLibraryUsageGuidelines.html#//apple_ref/doc/uid/TP40001928-SW9) can cause clashes between external libraries and those bundled in the `ThirdParty` directory of the Engine source tree, and under Linux where libraries need to build against the Engine's bundled version of libc++.


## Installation

{% include alerts/info.html content="ue4cli requires **Python 3.5 or newer**, and works as an interface to **Unreal Engine 4.19.0 or newer**." %}

To install ue4cli, simply run:

{% highlight bash %}
# This may need to be prefixed with sudo under Linux and macOS
pip3 install ue4cli
{% endhighlight %}

Under macOS, the old version of Mono that comes bundled with the Unreal Engine breaks when compiling .NET projects that have spaces in their paths, so you will need to install the latest version of Mono using [Homebrew](https://brew.sh/):

{% highlight bash %}
brew install mono
{% endhighlight %}


## Unreal Engine location auto-detection

{% capture _alert_content %}
Auto-detection is most useful when you are only working with a single version of the Unreal Engine and it is installed in the default location. If you have multiple Engine installations and want to control which version is used, or are working with installations in non-default locations (such as source builds of the Engine), then you should explicitly specify a path override using the [ue4 setroot](../configuration-commands/setroot) command.
{% endcapture %}
{% include alerts/info.html content=_alert_content %}

### Windows

The default installation directory used by the Epic Games Launcher is searched for the newest installed version:

{% highlight plaintext %}
%PROGRAMFILES%\Epic Games\UE_4.*
{% endhighlight %}

### macOS

The default installation directory used by the Epic Games Launcher is searched for the newest installed version:

{% highlight plaintext %}
/Users/Shared/Epic Games/UE_4.*
{% endhighlight %}

### Linux

Under Debian-based distributions, the Engine's shell integration is utilised to locate the root directory, by parsing the shortcut file:

{% highlight plaintext %}
~/.local/share/applications/UE4Editor.desktop
{% endhighlight %}

If this file doesn't exist, then the command `UE4Editor` needs to be in the system PATH.


## List of commands

{% assign categories = site.documents | where: "subsite", page.subsite | where: "layout", "command" | sort: "chapnum" | group_by: "chapnum" %}
{% for category in categories %}
{% assign name = category.items[0].chapter %}
{% assign blurb = category.items[0].chapter_blurb %}

### {{ name | escape }}

{{ blurb }}

{::nomarkdown}
{% assign commands = category.items | sort: "pagenum" %}
<ul class="detail-list">
{% for command in commands %}
	<li><p><a href="{{ command.url | relative_url | uri_escape }}">{{ command.title | escape }}</a></p><p>{{ command.blurb | escape }}</p></li>
{% endfor %}
</ul>
{:/}
{% endfor %}


## Links

- [ue4cli GitHub repository]({{ site.data.common.projects.ue4cli.repo }})
- [ue4cli package on PyPI](https://pypi.org/project/ue4cli/)
- [Related articles on adamrehn.com](https://adamrehn.com/articles/tag/Unreal%20Engine/)
